\section{Os Números Naturais}

\begin{frame}
  \frametitle{Naturais}

  Intuitivamente, temos

  \begin{center}
    \input{natural-number-line.tikz}
  \end{center}

  \pause

  ou\ldots

  \[
    \begin{split}
      \NN 
      & = \{0, 1, 2, \ldots\} \\
      & = \{0, 0 + 1, 0 + 1 + 1, \ldots\}\\
    \end{split}
  \]

  \pause

  Mas quem é 0? Quem é 1? Quem é 2? \ldots Desejamos definir tais valor 
  fazendo uso de \emph{objetos} que já conhecemos.
\end{frame}

\begin{frame}
  \frametitle{Naturais}

  \[
    \begin{split}
      \NN 
      & = \{0, 1, 2, \ldots\} \\
      & = \{0, 0 + 1, 0 + 1 + 1, \ldots\}\\
    \end{split}
  \]

  \pause

  \begin{itemize}
    \item \(0 = \emptyset\).
      \pause
    \item \(1 = ``0 + 1"\).
      \pause
    \item \(2 = ``1 + 1"\).
  \end{itemize}

  \pause

  Mas o quê é ``\(+ 1\)''?

  \pause

  \begin{itemize}
    \item \(``0 + 1'' = \{0\}\).
      \pause
    \item \(``1 + 1'' = \{0, 1\}\).
      \pause
    \item \(``2 + 1'' = \{0, 1, 2\}\).
  \end{itemize}

  \pause

  \begin{definition}
    Dado um conjunto \(X\), definimos o sucessor de \(X\) por 
    \(X \cup \{X\}\) -- denotado por \(S(X)\) ou \(X + 1\).
  \end{definition}
\end{frame}

\begin{frame}
  \frametitle{Naturais}

  \[
    \begin{split}
      \NN
      & = \{0, 0 + 1, 0 + 1 + 1, \ldots\} \\
      & = \{0, S(0), S(S(0)), \ldots \}
    \end{split}
  \]

  \pause

  \begin{example}
    \begin{align*}
      \begin{split}
        1
        & = S(0) \\
        & = 0 \cup \{0\} \\
        & = \emptyset \cup \{0\} \\
        & = \{0\} \\
      \end{split}
      &&
      \begin{split}
        2
        & = S(1) \\
        & = 1 \cup \{1\} \\
        & = \{0\} \cup \{1\} \\
        & = \{0, 1\}
      \end{split}
      &&
      \begin{split}
        3
        & = S(2) \\
        & = 2 \cup \{2\} \\
        & = \{0, 1\} \cup \{2\} \\
        & = \{0, 1, 2\}
      \end{split}
    \end{align*}
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{Conjuntos Indutivos}

  \begin{definition}
    Dado um conjunto \(X\), \(X\) é dito indutivo se, e somente se: 

    \begin{enumerate}
      \item \(\emptyset \in X\)
      \item \(x \in X \Rightarrow S(x) \in X\)
    \end{enumerate}
  \end{definition}

  \begin{example}
    \begin{enumerate}
      \item O conjunto \(\{\emptyset, S(\emptyset), S(S(\emptyset)), 
        \ldots\}\) é indutivo.

      \item O conjunto \(\{\emptyset, \bigstar, S(\emptyset), 
        S(\bigstar), \ldots\}\) é indutivo.
    \end{enumerate}
  \end{example}
\end{frame}

\begin{frame}
  \begin{axiom}
    Existe um conjunto indutivo.
  \end{axiom}

  \begin{definition}
    Seja \(I\) um conjunto indutivo. Então

    \[
      \NN 
      = \{x \in I : x \in X \text{ para todo } X \text{ indutivo }\}
    \]
  \end{definition}

  \pause

  \begin{fact}
    O conjunto \(\NN\) é indutivo.
  \end{fact}

  \pause

  \begin{fact}
    Dado \(X\) indutivo, \(\NN \subset X\).
  \end{fact}

  \pause

  \begin{fact}
    O conjunto \(\NN\) é \emph{indutivo-minimal} -- é o 
    \emph{menor} conjunto indutivo.
  \end{fact}
\end{frame}

\begin{frame}
  \frametitle{Ordem nos Naturais}

  Resumindo

  \[
    \begin{split}
      \NN
      & = \{0, 1, 2, \ldots\} \\
      & = \{0, 0 + 1, 0 + 1 + 1, \ldots\}\\
      & = \{\emptyset, \{0\}, \{0, 1\}, \ldots\}
    \end{split}
  \]

  \pause

  Perceba que

  \begin{multicols}{3}
    \[
      0 \in \{0\} = 1
    \]

    \[
      1 \in \{0, 1\} = 2
    \]

    \[
      2 \in \{0, 1, 2\} = 3
    \]
  \end{multicols}
\end{frame}

\begin{frame}
  \frametitle{Ordem nos Naturais}

  No geral, todo \(n \in \NN\) é dado por \(\{m \in \NN : m < n\}\). 

  \pause

  \begin{definition}
    Dados \(m, n \in \NN\), definimos

    \[
      m < n \iff m \in n
    \]
  \end{definition}

  \pause

  \begin{example}
    Os números \(0\), \(5\) e \(7\) são tais que \(0 \in \{0, 1, 2, 3, 
    4\} = 5\) e \(5 \in \{0, 1, 2, 3, 4, 5, 6\} = 7\). Logo, \(0 < 5 < 
    7\).
  \end{example}
\end{frame}

\begin{frame}
  \frametitle{Ordem nos Naturais}

  \begin{theorem}
    A relação \(<\) é uma relação de boa ordem em \(\NN\)

    \begin{enumerate}
      \item \(n \not< n\)
      \item \(n < m \text{ e } m < k \Rightarrow n < k\)
      \item \(n < m\) ou \(m < n\) ou \(n = m\)
      \item Para todo \(S \subset \NN\) não vazio, \(S\) 
        admite elemento mínimo.
    \end{enumerate}
  \end{theorem}

  \begin{center}
    \input{natural-number-line.tikz}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Ordem nos Naturais}

  \begin{theorem}
    A relação \(<\) é uma relação de boa ordem em \(\NN\)

    \begin{enumerate}
      \item \(0 \not< 0\)
      \item \(0 < 5 \text{ e } 5 < 7 \Rightarrow 0 < 7\)
      \item \(0 < 5\)
      \item \(\min \{5, 0, 7\} = 0\)
    \end{enumerate}
  \end{theorem}

  \begin{center}
    \input{natural-number-line.tikz}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Indução Finita}

  \begin{theorem}[PIF]
    Seja \(P\) uma propriedade a cerca dos números naturais. 
    Suponhamos que

    \begin{enumerate}
      \item Vale \(P(0)\).
      \item Se vale \(P(n)\) então vale \(P(n + 1)\).
    \end{enumerate}

    Então vale \(P(n)\) para todo \(n\).
  \end{theorem}

  \pause

  \begin{proof}
    Seja \(\NN_P = \{n \in \NN : P(n)\}\). Claramente, 
    \(\NN_P \subset \NN\). Ainda, afirmo que \(\NN_P\) é indutivo. De 
    fato

    \begin{enumerate}
      \item Como vale \(P(0)\), \(0 \in \NN_P\).
      \item Se \(n \in \NN_P\), então vale \(P(n)\). Logo, vale 
        \(P(n + 1)\) e, portanto, \(S(n) = n + 1 \in \NN_P\).
    \end{enumerate}

    Assim, como \(\NN_P\) é indutivo, \(\NN \subset \NN_P\). Em 
    conclusão, \(\NN_P = \NN\), isto é, para todo \(n\) vale 
    \(P(n)\).
  \end{proof}
\end{frame}

\begin{frame}
  \frametitle{Recursão (Finita)}

  \begin{example}
    Considere a função fatorial

    \begin{enumerate}
      \item \(0! = 1\)
      \item \((n + 1)! = (n + 1) \cdot n!\)
    \end{enumerate}

    O valor da função em \(n + 1\) é definido em termos do valor da 
    função em \(n\).
  \end{example}

  \pause

  \begin{theorem}[da Recursão]
    Dado um conjunto não vazio \(X\), podemos definir \(f : \NN 
    \rightarrow X\) tal que \(f(0) = c\) e \(f(n + 1) = g(n, f(n))\) 
    para \(c \in X\) e alguma \(g : \NN \times X 
    \rightarrow X\).
  \end{theorem}
\end{frame}

\begin{frame}
  \frametitle{Aritmética Natural}

  \begin{definition}
    Dados \(m, n \in \NN\), definimos

    \begin{multicols}{2}
      \begin{enumerate}
        \item \(n + 0 = n\)
        \item \(n + (m + 1) = (n + m) + 1\)
        \item \(n \cdot 0 = 0\)
        \item \(n \cdot (m + 1) = n \cdot m + n\)
      \end{enumerate}
    \end{multicols}
  \end{definition}
\end{frame}

